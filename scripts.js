let paragraph = document.querySelectorAll("p");
paragraph.forEach((paragraphs) => {
  paragraphs.style.background = "#ff0000";
});

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
if (optionsList.childNodes.length > 0) {
  optionsList.childNodes.forEach((childNodes) => {
    console.log(` name ${childNodes.nodeName} type ${childNodes.nodeType}`);
  });
}

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

let mainHeader = document.querySelector(".main-header");
// let headerElements = mainHeader.children;
let headerElements = mainHeader.querySelectorAll("*");
headerElements.forEach((element) => {
  element.classList.add("nav-item");
  console.log(element);
});


let sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach((element)=>{
  element.classList.remove('section-title')
})